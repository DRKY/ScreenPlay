<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL" sourcelanguage="en">
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>Aktualności</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Forum</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>Udziel się</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Warsztat Steam</translation>
    </message>
    <message>
        <source>Issue Tracker</source>
        <translation>Lista problemów</translation>
    </message>
    <message>
        <source>Reddit</source>
        <translation>Reddit</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>Otwórz w przeglądarce</translation>
    </message>
</context>
<context>
    <name>CreateSidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation type="unfinished">Tools Overview</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished">Video Import h264 (.mp4)</translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished">Video Import VP8 &amp; VP9 (.webm)</translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished">Video import (all types)</translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation type="unfinished">GIF Wallpaper</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation type="unfinished">QML Wallpaper</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation type="unfinished">HTML5 Wallpaper</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation type="unfinished">Website Wallpaper</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation type="unfinished">QML Widget</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation type="unfinished">HTML Widget</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>Importuj dowolny typ filmu</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>W zależności od konfiguracji Twojego urządzenia zalecamy przekonwertować tapetę do konkretnego kodeka wideo. Jeśli wydajność jest słaba w obu przypadkach, możesz wypróbować tapetę QML! Wspierane są następujące formaty wideo: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>Ustaw preferowany kodek wideo:</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>Suwak jakości. Mniejsza wartość oznacza lepszą jakość.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Otwórz dokumentację</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Wybierz plik</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>Wystąpił błąd!</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Skopiuj tekst do schowka</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>Wróć do tworzenia i wyślij raport o błędzie!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Generowanie obrazu podglądu...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Generowanie miniaturki podglądu...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generowanie 5-sekundowego podglądu wideo...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generowanie podglądu gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Konwertowanie dźwięku...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Konwertowanie wideo... Może to zająć trochę czasu!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>BŁĄD konwertowania wideo!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>BŁĄD analizowania filmu!</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>Konwertuj film na tapetę</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generowanie podglądu wideo...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nazwa (wymagane!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>Adres URL YouTube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Przerwij</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Zapisz tapetę...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>Głośność</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>Moment filmu</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation type="unfinished">Fill Mode</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Rozciągnięcie</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Wypełnij</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation type="unfinished">Contain</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="unfinished">Cover</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation type="unfinished">Scale_Down</translation>
    </message>
</context>
<context>
    <name>ExitPopup</name>
    <message>
        <source>Minimize ScreenPlay</source>
        <translation type="unfinished">Minimize ScreenPlay</translation>
    </message>
    <message>
        <source>Always minimize ScreenPlay</source>
        <translation type="unfinished">Always minimize ScreenPlay</translation>
    </message>
    <message>
        <source>You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</source>
        <translation type="unfinished">You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</translation>
    </message>
    <message>
        <source>Quit ScreenPlay now</source>
        <translation type="unfinished">Quit ScreenPlay now</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation>Importuj tapetę Gif</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>Przeciągnij tutaj plik *.gif lub naciśnij &apos;Wybierz plik&apos;.</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation>Wybierz swój gif</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nazwa tapety</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Utworzone przez</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Znaczniki</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation>Utwórz tapetę HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nazwa tapety</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Utworzone przez</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Licencja i znaczniki</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Obraz podglądu</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation>Utwórz widżet HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Nazwa widżetu</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Utworzone przez</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Znaczniki</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation>Analizowanie filmu...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Generowanie obrazu podglądu...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Generowanie miniaturki podglądu...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generowanie 5-sekundowego podglądu wideo...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generowanie podglądu gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Konwertowanie dźwięku...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Konwertowanie wideo... Może to zająć trochę czasu!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>BŁĄD konwertowania wideo!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>BŁĄD analizowania filmu!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>Importowanie filmu jako tapetę</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generowanie podglądu wideo...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nazwa (wymagane!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>Adres URL YouTube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Przerwij</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Zapisz tapetę...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation>Importuj film .webm</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>Importowanie pliku webm pozwala na ominięcie czasochłonnej konwersji. Jeśli rezultat importera ScreenPlay po użyciu opcji &apos;importuj i konwertuj film (dowolny typ)&apos; nie będzie dla Ciebie satysfakcjonujący, możesz przekonwertować film korzystając z bezpłatnego programu o otwartym źródle o nazwie HandBrake!</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>Nieprawidłowy typ pliku. Należy wybrać plik VP8 lub VP9 (*.webm)!</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>Przeciągnij tutaj plik *.webm lub naciśnij &apos;Wybierz plik&apos;.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Otwórz dokumentację</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Wybierz plik</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation>Analizowanie filmu...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Generowanie obrazu podglądu...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Generowanie miniaturki podglądu...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generowanie 5-sekundowego podglądu wideo...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generowanie podglądu gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Konwertowanie dźwięku...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Konwertowanie wideo... Może to zająć trochę czasu!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>BŁĄD konwertowania wideo!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>BŁĄD analizowania filmu!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>Importowanie filmu jako tapetę</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generowanie podglądu wideo...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nazwa (wymagane!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>Adres URL YouTube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Przerwij</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Zapisz tapetę...</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation>Importuj film .mp4</translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation>ScreenPlay V0.15 i nowsze mogą odtwarzać pliki *.mp4 (znane również jako h264). Może to poprawić wydajność na starszych systemach.</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation>Nieprawidłowy typ pliku. Należy użyć prawidłowy plik h264 (*.mp4)!</translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation>Przeciągnij tutaj plik *.mp4 lub naciśnij &apos;Wybierz plik&apos;.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Otwórz dokumentację</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Wybierz plik</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>Odświeżanie!</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation>Przesuń, aby odświeżyć!</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>Więcej tapet oraz widżetów dostępne przez Warsztat Steam!</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>Otwórz lokalizację pliku</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>Usuń element</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>Usuń poprzez Warsztat</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>Otwórz stronę Warsztatu</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>Czy na pewno chcesz usunąć ten element?</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished">Export</translation>
    </message>
    <message>
        <source>We only support adding one item at once.</source>
        <translation type="unfinished">We only support adding one item at once.</translation>
    </message>
    <message>
        <source>File type not supported. We only support &apos;.screenplay&apos; files.</source>
        <translation type="unfinished">File type not supported. We only support &apos;.screenplay&apos; files.</translation>
    </message>
    <message>
        <source>Import Content...</source>
        <translation type="unfinished">Import Content...</translation>
    </message>
    <message>
        <source>Export Content...</source>
        <translation type="unfinished">Export Content...</translation>
    </message>
</context>
<context>
    <name>InstalledNavigation</name>
    <message>
        <source>All</source>
        <translation type="unfinished">All</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="unfinished">Scenes</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>Uzyskaj bezpłatne widżety i tapety poprzez Warsztat Steam</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>Przeglądaj Warsztat Steam</translation>
    </message>
    <message>
        <source>Get content via our forum</source>
        <translation type="unfinished">Get content via our forum</translation>
    </message>
    <message>
        <source>Open the ScreenPlay forum</source>
        <translation type="unfinished">Open the ScreenPlay forum</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation>Konfiguracja tapety</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Usuń wybrane</translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation>Tapety</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widżety</translation>
    </message>
    <message>
        <source>Remove all </source>
        <translation>Usuń wszystkie </translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation>Ustaw kolor</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation>Należy wybrać kolor</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>Create</source>
        <translation>Utwórz</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation>Warsztat</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>Zainstalowane</translation>
    </message>
    <message>
        <source>Community</source>
        <translation>Społeczność</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <source>Mute/Unmute all Wallpaper</source>
        <translation>Wycisz/Anuluj wyciszenie wszystkich tapet</translation>
    </message>
    <message>
        <source>Pause/Play all Wallpaper</source>
        <translation>Wstrzymaj/Odtwórz wszystkie tapety</translation>
    </message>
    <message>
        <source>Configure Wallpaper</source>
        <translation>Konfiguruj tapetę</translation>
    </message>
    <message>
        <source>Support me on Patreon!</source>
        <translation>Wspieraj nas na Patreon!</translation>
    </message>
    <message>
        <source>Close All Content</source>
        <translation>Zamknij wszystkie treści</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation>Utwórz tapetę QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nazwa tapety</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Utworzone przez</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Licencja i znaczniki</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Obraz podglądu</translation>
    </message>
</context>
<context>
    <name>QMLWallpaperMain</name>
    <message>
        <source>My ScreenPlay Wallpaper 🚀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation>Utwórz widżet QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Nazwa widżetu</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Utworzone przez</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Znaczniki</translation>
    </message>
</context>
<context>
    <name>QMLWidgetMain</name>
    <message>
        <source>My Widget 🚀</source>
        <translation type="unfinished">My Widget 🚀</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation>Profil zapisany pomyślnie!</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation>NOWE</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay zostanie uruchomione przy starcie systemu Windows i ustawi dla Ciebie tapetę za każdym razem.</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation>Wysoki priorytet autostartu</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>Ta opcja nadaje ScreenPlay wyższy priorytet autostartu w porównaniu do innych aplikacji.</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation>Wysyłaj anonimowe raporty o awariach oraz statystyki</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation>Pomóż nam poprawić działanie oraz stabilność ScreenPlay. Wszystkie zgromadzone dane są w pełni anonimowe i używane tylko w celach rozwoju! Korzystamy z &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt;, aby gromadzić i analizować te dane. &lt;b&gt;Ogromne podziękowania dla nich&lt;/b&gt; za zapewnienie nam bezpłatnego wsparcia premium dla projektów o otwartym źródle!</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation>Ustaw lokalizację zapisu</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation>Ustaw lokalizację</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation>Ścieżka do pamięci jest pusta!</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>Uwaga: Zmiana tego katalogu nie ma wpływu na ścieżkę pobierania z warsztatu. ScreenPlay wspiera posiadanie tylko jednego folderu na treść!</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Język</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation>Ustaw język interfejsu ScreenPlay</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Motyw</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation>Zmień motyw na jasny/ciemny</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation>Systemowy</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Ciemny</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Jasny</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>Wydajność</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>Wstrzymaj renderowanie tapety wideo, gdy inna aplikacja jest na pierwszym planie</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>Wyłączamy renderowanie wideo (dźwięk zostaje!) dla najlepszej wydajności. W przypadku problemów, możesz wyłączyć tę funkcję tutaj. Wymaga ponownego uruchomienia tapety!</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation>Domyślny tryb wypełniania</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>To ustawienie określa, w jaki sposób film jest skalowany, aby dopasować go do obszaru docelowego.</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Rozciągnięcie</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Wypełnienie</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation type="unfinished">Contain</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="unfinished">Cover</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation type="unfinished">Scale-Down</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Informacje</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation>Dziękujemy za wypróbowanie ScreenPlay</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>Cześć, nazywam się Elias Steurer znany również jako Kelteseth i jestem programistą ScreenPlay. Dziękuję Ci za wypróbowanie mojego oprogramowania. Obserwuj mnie, aby być na bieżąco z aktualizacjami ScreenPlay:</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation>Wyświetl listę zmian</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation>Oprogramowanie zewnętrzne</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay nie powstałoby gdyby nie prace innych osób. Ogromne podziękowania dla: </translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation>Licencje</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation>Rejestry</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>Tutaj znajdziesz wyjaśnienie, jeśli ScreenPlay nie działa poprawnie. Wyświetla wszystkie rejestry oraz ostrzeżenia podczas działania.</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation>Pokaż rejestry</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation>Ochrona danych</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>Bardzo ostrożnie używamy danych w celu rozwoju ScreenPlay. Nie sprzedajemy oraz nie udostępniamy tych (anonimowych) informacji osobom trzecim!</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation>Prywatność</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Kopiuj tekst do schowka</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Set Wallpaper</source>
        <translation>Ustaw tapetę</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation>Ustaw widżet</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation>Nagłówek</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation>Wybierz monitor do wyświetlania treści</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation>Ustaw głośność</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Tryb wypełnienia</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Rozciągnięcie</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Wypełnienie</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation type="unfinished">Contain</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="unfinished">Cover</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation type="unfinished">Scale-Down</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation>Bezpłatne narzędzia ułatwiające tworzenie tapety</translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation>Poniżej znajdziesz dodatkowe narzędzia do tworzenia tapety poza tymi, które oferuje dla Ciebie ScreenPlay!</translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation type="unfinished">Create a Website Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nazwa tapety</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Utworzone przez</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Znaczniki</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Obraz podglądu</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation>Zapisywanie...</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation>Aktualności i lista zmian</translation>
    </message>
</context>
</TS>
